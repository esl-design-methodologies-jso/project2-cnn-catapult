in_img = open("image_data.hex").read().split()
out = []
filter = [0] * 81
filter[0:4] = [-1] * 5
filter[5:19] = [1] * 15
in_img = list(map(int, in_img))

for x in range(22):
    for y in range(28):
        temp = 0
        for i in range(9):
            for j in range(3):
                for z in range(3):
                    temp += in_img[(z * 900) + ((x + i) * 30) + (y + j)] * filter[(z * 27) + (i * 3) + j]
        if temp < 0:
            out.append(0)
        elif temp >= 1023:
            out.append(1023)
        else:
            out.append(temp)
print(out)
