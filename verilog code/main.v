module main(input clk, rst);

	wire [7:0] in_img_rsc_singleport_data_in;
	wire [11:0] in_img_rsc_singleport_addr;
	wire in_img_rsc_singleport_re;
	wire in_img_rsc_singleport_we;
	wire [7:0] in_img_rsc_singleport_data_out;
	wire [7:0] filter_rsc_singleport_data_in;
	wire [6:0] filter_rsc_singleport_addr;
	wire filter_rsc_singleport_re;
	wire filter_rsc_singleport_we;
	wire [7:0] filter_rsc_singleport_data_out;
	wire [9:0] out_rsc_singleport_data_in;
	wire [9:0] out_rsc_singleport_addr;
	wire out_rsc_singleport_re;
	wire out_rsc_singleport_we;
	wire [9:0] out_rsc_singleport_data_out;
	
	in_ram IN_RAM (
		.clk    		(clk),
		.rst    		(1'b0),
		.read_enable    (~in_img_rsc_singleport_re),
		.write_enable   (~in_img_rsc_singleport_we),
		.in_address		(in_img_rsc_singleport_addr),
		.data_in   		(in_img_rsc_singleport_data_in),
		.data_out  		(in_img_rsc_singleport_data_out)
	);

	out_ram OUT_RAM (
		.clk    		(clk),
		.rst    		(rst),
		.read_enable    (~out_rsc_singleport_re),
		.write_enable   (~out_rsc_singleport_we),
		.in_address		(out_rsc_singleport_addr),
		.data_in   		(out_rsc_singleport_data_in),
		.data_out  		(out_rsc_singleport_data_out)
	);

	filter_ram FILTER_RAM(
		.clk    		(clk),
		.rst    		(rst),
		.read_enable    (~filter_rsc_singleport_re),
		.write_enable   (~filter_rsc_singleport_we),
		.in_address		(filter_rsc_singleport_addr),
		.data_in   		(filter_rsc_singleport_data_in),
		.data_out  		(filter_rsc_singleport_data_out)
	);

	cnn CNN(
		.clk                             (clk),
		.rst                             (rst),
		.in_img_rsc_singleport_data_in   (in_img_rsc_singleport_data_in),
		.in_img_rsc_singleport_addr      (in_img_rsc_singleport_addr),
		.in_img_rsc_singleport_re        (in_img_rsc_singleport_re),
		.in_img_rsc_singleport_we        (in_img_rsc_singleport_we),
		.in_img_rsc_singleport_data_out  (in_img_rsc_singleport_data_out),
		.filter_rsc_singleport_data_in   (filter_rsc_singleport_data_in),
		.filter_rsc_singleport_addr      (filter_rsc_singleport_addr),
		.filter_rsc_singleport_re        (filter_rsc_singleport_re),
		.filter_rsc_singleport_we        (filter_rsc_singleport_we),
		.filter_rsc_singleport_data_out  (filter_rsc_singleport_data_out),
		.out_rsc_singleport_data_in      (out_rsc_singleport_data_in),
		.out_rsc_singleport_addr         (out_rsc_singleport_addr),
		.out_rsc_singleport_re           (out_rsc_singleport_re),
		.out_rsc_singleport_we           (out_rsc_singleport_we),
		.out_rsc_singleport_data_out     (out_rsc_singleport_data_out)
	);


endmodule

module tb();

	reg clk, rst;
  
  	main m(clk, rst);
  	initial begin
	  	clk=1;
    	repeat (350000)
    	#50 clk=~clk;
	end
	
	initial begin
	  	rst = 1;
	  	#50;
	  	rst = 0;
	  	#100;
	end

endmodule