library verilog;
use verilog.vl_types.all;
entity \all\ is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic
    );
end \all\;
