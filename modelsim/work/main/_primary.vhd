library verilog;
use verilog.vl_types.all;
entity main is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic
    );
end main;
