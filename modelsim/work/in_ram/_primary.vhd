library verilog;
use verilog.vl_types.all;
entity in_ram is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        read_enable     : in     vl_logic;
        write_enable    : in     vl_logic;
        in_address      : in     vl_logic_vector(11 downto 0);
        data_in         : in     vl_logic_vector(7 downto 0);
        data_out        : out    vl_logic_vector(7 downto 0)
    );
end in_ram;
