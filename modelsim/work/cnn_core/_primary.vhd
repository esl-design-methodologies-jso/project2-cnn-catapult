library verilog;
use verilog.vl_types.all;
entity cnn_core is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        in_img_rsc_singleport_addr: out    vl_logic_vector(11 downto 0);
        in_img_rsc_singleport_re: out    vl_logic;
        in_img_rsc_singleport_data_out: in     vl_logic_vector(7 downto 0);
        filter_rsc_singleport_addr: out    vl_logic_vector(6 downto 0);
        filter_rsc_singleport_re: out    vl_logic;
        filter_rsc_singleport_data_out: in     vl_logic_vector(7 downto 0);
        out_rsc_singleport_data_in: out    vl_logic_vector(9 downto 0);
        out_rsc_singleport_addr: out    vl_logic_vector(9 downto 0);
        out_rsc_singleport_we: out    vl_logic
    );
end cnn_core;
