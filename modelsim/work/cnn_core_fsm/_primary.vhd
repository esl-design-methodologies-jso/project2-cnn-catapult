library verilog;
use verilog.vl_types.all;
entity cnn_core_fsm is
    generic(
        st_main         : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi0, Hi0);
        st_DEPTH        : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi0, Hi1);
        st_DEPTH_1      : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi1, Hi0);
        st_DEPTH_2      : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi1, Hi1);
        st_FILTERCOLUMN : vl_logic_vector(0 to 3) := (Hi0, Hi1, Hi0, Hi0);
        st_FILTERROW    : vl_logic_vector(0 to 3) := (Hi0, Hi1, Hi0, Hi1);
        st_INCOLUMN     : vl_logic_vector(0 to 3) := (Hi0, Hi1, Hi1, Hi0);
        st_INCOLUMN_1   : vl_logic_vector(0 to 3) := (Hi0, Hi1, Hi1, Hi1);
        st_INROW        : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi0, Hi0);
        state_x         : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi0, Hi0)
    );
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        fsm_output      : out    vl_logic_vector(8 downto 0);
        st_DEPTH_2_tr0  : in     vl_logic;
        st_FILTERCOLUMN_tr0: in     vl_logic;
        st_FILTERROW_tr0: in     vl_logic;
        st_INCOLUMN_1_tr0: in     vl_logic;
        st_INROW_tr0    : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of st_main : constant is 1;
    attribute mti_svvh_generic_type of st_DEPTH : constant is 1;
    attribute mti_svvh_generic_type of st_DEPTH_1 : constant is 1;
    attribute mti_svvh_generic_type of st_DEPTH_2 : constant is 1;
    attribute mti_svvh_generic_type of st_FILTERCOLUMN : constant is 1;
    attribute mti_svvh_generic_type of st_FILTERROW : constant is 1;
    attribute mti_svvh_generic_type of st_INCOLUMN : constant is 1;
    attribute mti_svvh_generic_type of st_INCOLUMN_1 : constant is 1;
    attribute mti_svvh_generic_type of st_INROW : constant is 1;
    attribute mti_svvh_generic_type of state_x : constant is 1;
end cnn_core_fsm;
