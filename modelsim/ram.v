module in_ram(
	input clk, rst,
	input read_enable, write_enable,
	input [11:0] in_address,
	input [7:0] data_in,
	output reg[7:0] data_out
);
	reg [7:0] ram [0: 2699];
	initial $readmemh("image_data_hex.hex", ram);

	integer i;
	always @(posedge clk) begin
		if (rst)
			for (i = 0; i < 2700; i = i + 1)
				ram[i] <= 8'b0;
		else if (write_enable)
			ram[in_address] <= data_in;
		else if (read_enable)
			data_out <= ram[in_address];
	end
endmodule

module out_ram(
	input clk, rst,
	input read_enable, write_enable,
	input [9:0] in_address,
	input [9:0] data_in,
	output reg[9:0] data_out
);
	reg [9:0] ram [0: 615];

	integer i;
	always @(posedge clk) begin
		if (rst)
			for (i = 0; i < 616; i = i + 1)
				ram[i] <= 10'b0;
		else if (write_enable)
			ram[in_address] <= data_in;
		else if (read_enable)
			data_out <= ram[in_address];
	end
endmodule

module filter_ram(
	input clk, rst,
	input read_enable, write_enable,
	input [6:0] in_address,
	input [7:0] data_in,
	output reg signed [7:0] data_out
);
	reg [7:0] ram [0: 80];

	integer i;
	always @(posedge clk) begin
		if (rst)
			for (i = 0; i < 81; i = i + 1)
				ram[i] <= 8'b0;
		else if (write_enable)
			ram[in_address] <= data_in;
		else if (read_enable)
			data_out <= ram[in_address];

		ram[0]  <= -1;ram[1]  <= -1;ram[2]  <= -1;ram[3]  <= -1;ram[4]  <= -1;
		ram[5]  <= 1;ram[6]  <= 1;ram[7]  <= 1;ram[8]  <= 1;ram[9]  <= 1;
		ram[10] <= 1;ram[11] <= 1;ram[12] <= 1;ram[13] <= 1;ram[14] <= 1;
		ram[15] <= 1;ram[16] <= 1;ram[17] <= 1;ram[18] <= 1;ram[19] <= 1;

		ram[20] <= 0;ram[21] <= 0;ram[22] <= 0;ram[23] <= 0;ram[24] <= 0;
		ram[25] <= 0;ram[26] <= 0;ram[27] <= 0;ram[28] <= 0;ram[29] <= 0;
		ram[30] <= 0;ram[31] <= 0;ram[32] <= 0;ram[33] <= 0;ram[34] <= 0;
		ram[35] <= 0;ram[36] <= 0;ram[37] <= 0;ram[38] <= 0;ram[39] <= 0;

		ram[40] <= 0;ram[41] <= 0;ram[42] <= 0;ram[43] <= 0;ram[44] <= 0;
		ram[45] <= 0;ram[46] <= 0;ram[47] <= 0;ram[48] <= 0;ram[49] <= 0;
		ram[50] <= 0;ram[51] <= 0;ram[52] <= 0;ram[33] <= 0;ram[54] <= 0;
		ram[55] <= 0;ram[56] <= 0;ram[57] <= 0;ram[58] <= 0;ram[59] <= 0;

		ram[60] <= 0;ram[61] <= 0;ram[62] <= 0;ram[63] <= 0;ram[64] <= 0;
		ram[65] <= 0;ram[66] <= 0;ram[67] <= 0;ram[68] <= 0;ram[69] <= 0;
		ram[70] <= 0;ram[71] <= 0;ram[72] <= 0;ram[73] <= 0;ram[74] <= 0;
		ram[75] <= 0;ram[76] <= 0;ram[77] <= 0;ram[78] <= 0;ram[79] <= 0;
	end

	always @(*) begin
		ram[0]  <= 1;ram[1]  <= 1;ram[2]  <= 1;ram[3]  <= 1;ram[4]  <= 1;
		ram[5]  <= 1;ram[6]  <= 1;ram[7]  <= 1;ram[8]  <= 1;ram[9]  <= 1;
		ram[10] <= 1;ram[11] <= 1;ram[12] <= 1;ram[13] <= 1;ram[14] <= 1;
		ram[15] <= 1;ram[16] <= 1;ram[17] <= 1;ram[18] <= 1;ram[19] <= 1;

		ram[20] <= 0;ram[21] <= 0;ram[22] <= 0;ram[23] <= 0;ram[24] <= 0;
		ram[25] <= 0;ram[26] <= 0;ram[27] <= 0;ram[28] <= 0;ram[29] <= 0;
		ram[30] <= 0;ram[31] <= 0;ram[32] <= 0;ram[33] <= 0;ram[34] <= 0;
		ram[35] <= 0;ram[36] <= 0;ram[37] <= 0;ram[38] <= 0;ram[39] <= 0;

		ram[40] <= 0;ram[41] <= 0;ram[42] <= 0;ram[43] <= 0;ram[44] <= 0;
		ram[45] <= 0;ram[46] <= 0;ram[47] <= 0;ram[48] <= 0;ram[49] <= 0;
		ram[50] <= 0;ram[51] <= 0;ram[52] <= 0;ram[33] <= 0;ram[54] <= 0;
		ram[55] <= 0;ram[56] <= 0;ram[57] <= 0;ram[58] <= 0;ram[59] <= 0;

		ram[60] <= 0;ram[61] <= 0;ram[62] <= 0;ram[63] <= 0;ram[64] <= 0;
		ram[65] <= 0;ram[66] <= 0;ram[67] <= 0;ram[68] <= 0;ram[69] <= 0;
		ram[70] <= 0;ram[71] <= 0;ram[72] <= 0;ram[73] <= 0;ram[74] <= 0;
		ram[75] <= 0;ram[76] <= 0;ram[77] <= 0;ram[78] <= 0;ram[79] <= 0;
	end
endmodule