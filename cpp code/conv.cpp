#include <ac_int.h>

void cnn(ac_int<8,false> in_img[2700], ac_int<8,true> filter[81], ac_int<10,false> out[616]){
	ac_int<10,false> temp;

	INROW: for(int x = 0; x < 22; x++) {
		INCOLUMN: for(int y = 0; y < 28; y++) {
			temp = 0;
			FILTERROW: for(int i = 0; i < 9; i++) {
				FILTERCOLUMN: for(int j = 0; j < 3; j++) {
					DEPTH: for(int z = 0; z < 3; z++) {
						temp += in_img[(z * 900) + ((x + i) * 30) + (y + j)] * filter[(z * 27) + (i * 3) + j];
					}
				}
			}
			out[(x * 30) + y] = (temp < 0) ? 0 : (temp >= 1023) ? 1023 : temp;
		}
	}
}
